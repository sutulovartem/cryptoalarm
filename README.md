Crypto-alarm (2017)
===============

## Description
Crypto-alarm it's an application for viewing crypto currencies. It was made by me, Artem Sutulov
and an starting purposes were:  
 * Viewing currencies;  
 * Adding favorites currencies;  
 * Adding 'alarm' on the currency price changing event (it's helpful for trading).  

I didn't realize all the goals of the application, because it was my study project and when I was
developing this app I was learning android development from scratch. When i got some knowledge
about android development, I stopped working on this app because I've lost interest.

Technology which were used:  
 * Kotlin as language instead java;  
 * Rx-java for organizing async requests;  
 * Retrofit for using coin-market-cap api;  
 * Material design;  
 * MVP pattern;  

Technology which were used for study purposes:  
 * Google firebase;  
 * Realm storage;  

## What app can do
App can do:  
 * View all currencies (picture parsing is not working because client api has been changed);
(See (doc/1.png))  
* View current currency details (frontend part is not fully realized);  
(See (doc/2.png))  
* Search currencies by full name;
See(doc/3.png)  
* View current currency chart (it's now working because client api has been changed).
(See (doc/4.png))  

Also, app has partition-async currencies loading (See (doc/5.png)).

