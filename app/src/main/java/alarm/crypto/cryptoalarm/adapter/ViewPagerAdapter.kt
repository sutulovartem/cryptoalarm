package alarm.crypto.cryptoalarm.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * @autor artemsutulov, 04.12.2017.
 */
class ViewPagerAdapter(manager: FragmentManager?) : FragmentPagerAdapter(manager) {

    private var fragments: ArrayList<Fragment> = arrayListOf()

    private var fragmentsTitles: ArrayList<String> = arrayListOf()

    override fun getItem(position: Int) = fragments[position]

    override fun getPageTitle(position: Int) = fragmentsTitles[position]

    override fun getCount() = fragments.size


    fun addFragment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        fragmentsTitles.add(title)
    }

}