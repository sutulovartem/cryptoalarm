package alarm.crypto.cryptoalarm.adapter

import alarm.crypto.cryptoalarm.R
import alarm.crypto.cryptoalarm.event.OnCurrencyClick
import alarm.crypto.cryptoalarm.fragment.menu.HomeFragment.Companion.ELEMENTS_STACK_TO_LOAD_COUNT
import alarm.crypto.cryptoalarm.presenter.IHomeFragmentPresenter
import alarm.crypto.cryptoalarm.retrofit.CoinMarketCapApiContainer
import alarm.crypto.cryptoalarm.retrofit.CryptoCurrency
import android.content.Context
import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide

class CurrencyRecycleViewAdapter(
        private val visibleCurrencies: ArrayList<CryptoCurrency>?,
        private val allCurrencies: ArrayList<CryptoCurrency>?,
        private val homePresenter: IHomeFragmentPresenter,
        private val onClick: OnCurrencyClick,
        private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    companion object {
        val VIEW_TYPE_ITEM = 0
        val VIEW_TYPE_LOADING = 1
    }

    private var isLoading: Boolean = false

    private var isEndOfListCurrencies = false

    override fun getItemCount() = visibleCurrencies?.size ?: 0


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder? {
        if (viewType == VIEW_TYPE_ITEM) {
            val li = LayoutInflater
                    .from(parent.context).inflate(R.layout.currency_item, parent, false)
            return CurrencyViewHolder(li, onClick)
        } else if (viewType == VIEW_TYPE_LOADING) {
            val li = LayoutInflater.from(parent.context)
                    .inflate(R.layout.loading_item, parent, false)
            return LoadingViewHolder(li)
        }
        return null
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CurrencyViewHolder) {
            val currency = visibleCurrencies!![position]
            holder.rank.text = currency.rank
            holder.currencyValue.text = currency.priceUsd
            holder.currencyName.text = currency.symbol
            val dayPercentChangedValue = currency.dayPercentChanged
            holder.currencyValueDay.text = dayPercentChangedValue
            if (dayPercentChangedValue!!.toDouble() > 0) {  //FIXME NPE
                holder.currencyValueDay.setTextColor(Color.GREEN)
            } else {
                holder.currencyValueDay.setTextColor(Color.RED)
            }
            Glide.with(context).load(CoinMarketCapApiContainer.URL_FOR_GET_PNG +
                    currency.id + ".png").into(holder.currencyIcon)
        } else if (holder is LoadingViewHolder) {
            if (isLoading) {
                holder.loadingItem.visibility = View.VISIBLE
                holder.loadingItem.isIndeterminate = true
            }
        } else (holder as LoadingViewHolder).loadingItem.visibility = View.GONE
    }


    class CurrencyViewHolder(
            itemView: View,
            private val onClick: OnCurrencyClick
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var rank: TextView = itemView.findViewById(R.id.rank)
        var currencyName: TextView = itemView.findViewById(R.id.currency_name)
        var currencyValue: TextView = itemView.findViewById(R.id.currency_value)
        var currencyValueDay: TextView = itemView.findViewById(R.id.percent_changed_day)
        var currencyIcon: ImageView = itemView.findViewById(R.id.currency_icon)

        init {
            itemView.setOnClickListener(this)
        }


        override fun onClick(v: View) {
            onClick.onCurrencyItemClick(v, adapterPosition)
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var loadingItem: ProgressBar = itemView.findViewById(R.id.loading_item)
    }

    fun loadContent(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        val totalItemCount = layoutManager.itemCount
        val lastVisibleItem = layoutManager.findLastVisibleItemPosition() + 1
        if (!isLoading && lastVisibleItem == totalItemCount) {
            isLoading = true
            visibleCurrencies!!.add(CryptoCurrency())
            recyclerView.post { notifyItemInserted(visibleCurrencies.size - 1) }
            homePresenter.loadMoreCurrencies(ELEMENTS_STACK_TO_LOAD_COUNT, visibleCurrencies.size)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val element = visibleCurrencies!![position]
        return if (element.isEmpty()) {
            VIEW_TYPE_LOADING
        } else
            VIEW_TYPE_ITEM
    }

    override fun getFilter() = object : Filter() {
        override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
        }

        override fun performFiltering(charSequence: CharSequence?): FilterResults {
            homePresenter.search(charSequence.toString())
            return FilterResults()
        }
    }

    fun setLoaded() {
        isLoading = false
    }

    fun getVisibleCurrencies() = visibleCurrencies
}
