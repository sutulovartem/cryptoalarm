package alarm.crypto.cryptoalarm.presenter

/**
 * @autor artemsutulov, 14.12.2017.
 */
interface IHomeFragmentPresenter {
    fun initCurrencies(currenciesCountToLoad: Int)
    fun loadAllCurrencies()
    fun loadMoreCurrencies(currenciesCountToAdd: Int, startWith: Int)
    fun refreshCurrencies(currenciesCountToLoad: Int)
    fun search(searchString: String)
    fun sort(fieldBySort: String)
}