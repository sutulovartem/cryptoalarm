package alarm.crypto.cryptoalarm.presenter

import alarm.crypto.cryptoalarm.fragment.menu.HomeFragment.Companion.ELEMENTS_STACK_TO_LOAD_COUNT
import alarm.crypto.cryptoalarm.fragment.menu.IHomeFragmentView
import alarm.crypto.cryptoalarm.retrofit.CoinMarketCapCurrenciesApi
import alarm.crypto.cryptoalarm.retrofit.CryptoCurrency
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @autor artemsutulov, 14.12.2017.
 */
class HomeFragmentPresenter(private val view: IHomeFragmentView) : IHomeFragmentPresenter {

    override fun loadAllCurrencies() {
        api.getCurrenciesWithLimit(Int.MAX_VALUE).enqueue(object : Callback<List<CryptoCurrency>> {

            override fun onResponse(call: Call<List<CryptoCurrency>>,
                                    response: Response<List<CryptoCurrency>>) {
                if (response.isSuccessful) {
                    view.clearAndAddCurrenciesToArrayAllCurrencies(
                            response.body() as ArrayList<CryptoCurrency>
                    )
                }
            }

            override fun onFailure(call: Call<List<CryptoCurrency>>, t: Throwable) {
                view.showErrorMessage("Can not load currencies")
            }

        })
    }

    private val api: CoinMarketCapCurrenciesApi = Retrofit.Builder()
            .baseUrl("https://api.coinmarketcap.com/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(CoinMarketCapCurrenciesApi::class.java)

    override fun initCurrencies(currenciesCountToLoad: Int) {
        api.getCurrenciesWithLimit(ELEMENTS_STACK_TO_LOAD_COUNT)
                .enqueue(object : Callback<List<CryptoCurrency>> {

                    override fun onResponse(call: Call<List<CryptoCurrency>>,
                                            response: Response<List<CryptoCurrency>>) {
                        if (response.isSuccessful) {
                            view.clearAndAddAllCurrencies(
                                    response.body() as ArrayList<CryptoCurrency>
                            )
                        }
                    }

                    override fun onFailure(call: Call<List<CryptoCurrency>>, t: Throwable) {
                        view.showErrorMessage("FIXME")
                    }

                })
    }

    override fun refreshCurrencies(currenciesCountToLoad: Int) {
        api.getCurrenciesWithLimit(currenciesCountToLoad)
                .enqueue(object : Callback<List<CryptoCurrency>> {

                    override fun onResponse(call: Call<List<CryptoCurrency>>,
                                            response: Response<List<CryptoCurrency>>) {
                        if (response.isSuccessful) {
                            view.clearAndAddAllCurrencies(
                                    response.body() as ArrayList<CryptoCurrency>
                            )
                            view.notifyContentRefreshed()
                        }
                    }

                    override fun onFailure(call: Call<List<CryptoCurrency>>, t: Throwable) {
                        view.showErrorMessage("FIXME")
                    }

                })
    }

    override fun loadMoreCurrencies(currenciesCountToAdd: Int, startWith: Int) {
        api.getCurrenciesStartWithAndWithLimit(ELEMENTS_STACK_TO_LOAD_COUNT, startWith)
                .enqueue(object : Callback<List<CryptoCurrency>> {

                    override fun onResponse(call: Call<List<CryptoCurrency>>,
                                            response: Response<List<CryptoCurrency>>) {
                        view.addMoreContent(
                                response.body() as ArrayList<CryptoCurrency>, true
                        )
                    }

                    override fun onFailure(call: Call<List<CryptoCurrency>>, t: Throwable) {
                        view.showErrorMessage("FIXME")
                    }

                })
    }

    override fun search(searchString: String) {
        view.searchAndUpdateData(searchString)
    }

    override fun sort(fieldBySort: String) {
    }
}