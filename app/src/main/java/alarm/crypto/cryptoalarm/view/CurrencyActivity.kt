package alarm.crypto.cryptoalarm.view

import alarm.crypto.cryptoalarm.R
import alarm.crypto.cryptoalarm.adapter.ViewPagerAdapter
import alarm.crypto.cryptoalarm.fragment.AboutCurrencyFragment
import alarm.crypto.cryptoalarm.fragment.ChartFragment
import alarm.crypto.cryptoalarm.retrofit.CryptoCurrency
import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView

class CurrencyActivity : AppCompatActivity() {

    companion object {
        val EXTRA_CURRENCY_NAME: String = "currency_position"
        val CURRENCY_KEY_FOR_FRAGMENT = "CRYPTO_CURRENCY"
    }

    private lateinit var currency: CryptoCurrency

    private lateinit var toolBar: Toolbar
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency)
        toolBar = findViewById(R.id.toolbar)
        setSupportActionBar(toolBar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        currency = intent.getParcelableExtra(EXTRA_CURRENCY_NAME)
        viewPager = findViewById(R.id.viewpager)
        viewPager.adapter = getViewPagerAdapter()
        tabLayout = findViewById(R.id.tabs)
        tabLayout.setupWithViewPager(viewPager)
    }

    @SuppressLint("WrongViewCast")
    override fun onStart() {
        super.onStart()
        findViewById<TextView>(R.id.toolbar_currency_activity_currencies_name).text = currency.name
    }

    private fun getViewPagerAdapter(): PagerAdapter {
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        val aboutCurrencyFragment = AboutCurrencyFragment()
        val bundle = Bundle()
        bundle.putParcelable(CURRENCY_KEY_FOR_FRAGMENT, currency)
        aboutCurrencyFragment.arguments = bundle
        viewPagerAdapter.addFragment(aboutCurrencyFragment, "Info")
        val chartFragment = ChartFragment()
        chartFragment.arguments = bundle
        viewPagerAdapter.addFragment(chartFragment, "Chart")
        return viewPagerAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)
        return true
    }

}