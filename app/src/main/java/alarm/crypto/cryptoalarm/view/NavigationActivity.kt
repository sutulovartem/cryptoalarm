package alarm.crypto.cryptoalarm.view

import alarm.crypto.cryptoalarm.Cache
import alarm.crypto.cryptoalarm.R
import alarm.crypto.cryptoalarm.fragment.menu.FavoriteFragment
import alarm.crypto.cryptoalarm.fragment.menu.HomeFragment
import alarm.crypto.cryptoalarm.fragment.menu.HomeFragment.Companion.CACHED_INSTANCE_CURRENCIES_LIST_KEY
import alarm.crypto.cryptoalarm.fragment.menu.HomeFragment.Companion.ELEMENTS_STACK_KEY
import alarm.crypto.cryptoalarm.fragment.menu.TopFragment
import alarm.crypto.cryptoalarm.retrofit.CryptoCurrency
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.widget.SearchView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.roughike.bottombar.BottomBar
import java.util.*


class NavigationActivity : AppCompatActivity() {

    companion object {
        private const val isNeedFireBaseAuth = false

        const val BOTTOM_BAR_INSTANCE_KEY = "bottomBarInstance"

        private val AUTH: FirebaseAuth = FirebaseAuth.getInstance()

        //TODO unused integration with google firebase
        private val USER: FirebaseUser? = getAuthUser()

        private fun getAuthUser(): FirebaseUser? {
            var user = AUTH.currentUser
            if (isNeedFireBaseAuth && user == null) {
                AUTH.signInAnonymously().addOnCompleteListener {
                    if (it.isSuccessful) {
                        user = AUTH.currentUser
                    }
                }
            }
            return user
        }
    }

    private lateinit var toolBar: Toolbar

    private lateinit var bottomBar: BottomBar

    private lateinit var homeFragment: HomeFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        bottomBar = findViewById(R.id.bottomBar)
        homeFragment = HomeFragment()
        bottomBar.setOnTabSelectListener { tabId ->
            when (tabId) {
                R.id.tab_home -> {
                    val arguments = Bundle()
                    val currencies = Cache.getFromCache(CACHED_INSTANCE_CURRENCIES_LIST_KEY)
                    if (currencies != null) {
                        arguments.putParcelableArrayList(ELEMENTS_STACK_KEY, currencies as ArrayList<CryptoCurrency>)
                    }
                    pushFragment(homeFragment, arguments)
                }
                R.id.tab_favorite -> pushFragment(FavoriteFragment(), Bundle())
                R.id.tab_top -> pushFragment(TopFragment(), Bundle())
            }
        }
        toolBar = findViewById(R.id.toolbar)
        setSupportActionBar(toolBar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putParcelable(BOTTOM_BAR_INSTANCE_KEY, bottomBar.onSaveInstanceState())
    }


    private fun pushFragment(fragment: Fragment, bundle: Bundle) {
        val transaction = supportFragmentManager.beginTransaction()
        fragment.arguments = bundle
        transaction.replace(R.id.rootLayout, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.navigatoin_activity_toolbar, menu)
        val search = menu!!.findItem(R.id.navigation_activity_search_bar)
        val searchView = MenuItemCompat.getActionView(search) as SearchView
        search(searchView)
        return true
    }

    private fun search(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                homeFragment.getAdapter().filter.filter(newText)
                return true
            }
        })
    }

}
