package alarm.crypto.cryptoalarm.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CoinMarketCapGraphAdi {
    @GET("currencies/{currencyName}")
    fun getGraphDataByCurrencyName(@Path("currencyName") currencyName: String): Call<CryptoCurrencyGraphData>
}