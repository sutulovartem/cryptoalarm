package alarm.crypto.cryptoalarm.retrofit

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @autor artemsutulov, 16.11.2017.
 */
class CoinMarketCapApiContainer {
    companion object {
        val URL_FOR_GET_PNG: String = "https://files.coinmarketcap.com/static/img/coins/64x64/"
        private var graphAdi: CoinMarketCapGraphAdi? = null

        fun getGraphApi(): CoinMarketCapGraphAdi {
            if (graphAdi == null) initCoinMarketCapGrapthApi()
            return graphAdi!!
        }

        private fun initCoinMarketCapGrapthApi() {
            val retrofit = Retrofit.Builder()
                    .baseUrl("https://graphs.coinmarketcap.com/")
                    .addConverterFactory(GsonConverterFactory.create(
                            GsonBuilder()
                                    .registerTypeAdapter(
                                            CryptoCurrencyGraphData::class.java,
                                            JsonDeserializer { json, typeOfT, context ->
                                                val gson = Gson()
                                                gson
                                                        .fromJson(
                                                                json,
                                                                CryptoCurrencyGraphData::class.java
                                                        )
                                            }
                                    )
                                    .setLenient()
                                    .create()
                    )
                    )
                    .build()
            graphAdi = retrofit.create(CoinMarketCapGraphAdi::class.java)
        }
    }
}