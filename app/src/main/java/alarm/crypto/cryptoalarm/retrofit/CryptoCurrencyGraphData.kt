package alarm.crypto.cryptoalarm.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * @autor artemsutulov, 16.11.2017.
 */
data class CryptoCurrencyGraphData(
        @SerializedName("market_cap_by_available_supply")
        @Expose
        var marketCapByAvailableSupply: List<List<Long>>? = null,
        @SerializedName("price_usd")
        @Expose
        var priceUsd: List<List<Long>>? = null,
        @SerializedName("volume_usd")
        @Expose
        var volumeUsd: List<List<Long>>? = null,
        @SerializedName("price_btc")
        @Expose
        var priceBtc: List<List<Long>>? = null
)