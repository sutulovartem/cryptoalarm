package alarm.crypto.cryptoalarm.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CoinMarketCapCurrenciesApi {
    @GET("ticker")
    fun getCurrenciesWithLimit(@Query("limit") limit: Int): Call<List<CryptoCurrency>>

    @GET("ticker")
    fun getCurrenciesStartWithAndWithLimit(@Query("limit") limit: Int,@Query("start") startWith: Int): Call<List<CryptoCurrency>>

    @GET("v1/ticker/{currency}")
    fun getCurrency(@Path("currency") currencyName: String): Call<CryptoCurrency>
}
