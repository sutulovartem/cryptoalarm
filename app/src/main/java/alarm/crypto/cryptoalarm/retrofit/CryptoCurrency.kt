package alarm.crypto.cryptoalarm.retrofit

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CryptoCurrency(
        @SerializedName("id")
        @Expose var id: String? = null,
        @SerializedName("name")
        @Expose var name: String? = null,
        @SerializedName("symbol")
        @Expose var symbol: String? = null,
        @SerializedName("rank")
        @Expose var rank: String? = null,
        @SerializedName("price_usd")
        @Expose var priceUsd: String? = null,
        @SerializedName("price_btc")
        @Expose var priceBtc: String? = null,
        @SerializedName("24h_volume_usd")
        @Expose var dayVolumeUsd: String? = null,
        @SerializedName("market_cap_usd")
        @Expose var marketCapUsd: String? = null,
        @SerializedName("available_supply")
        @Expose var availableSupply: String? = null,
        @SerializedName("total_supply")
        @Expose var totalSupply: String? = null,
        @SerializedName("percent_change_1h")
        @Expose var hourPercentChanged: String? = null,
        @SerializedName("percent_change_24h")
        @Expose var dayPercentChanged: String? = null,
        @SerializedName("percent_change_7d")
        @Expose var weekPercentChanged: String? = null,
        @SerializedName("last_updated")
        @Expose var lastUpdated: String? = null
): Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString())

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(id)
                parcel.writeString(name)
                parcel.writeString(symbol)
                parcel.writeString(rank)
                parcel.writeString(priceUsd)
                parcel.writeString(priceBtc)
                parcel.writeString(dayVolumeUsd)
                parcel.writeString(marketCapUsd)
                parcel.writeString(availableSupply)
                parcel.writeString(totalSupply)
                parcel.writeString(hourPercentChanged)
                parcel.writeString(dayPercentChanged)
                parcel.writeString(weekPercentChanged)
                parcel.writeString(lastUpdated)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<CryptoCurrency> {
                override fun createFromParcel(parcel: Parcel): CryptoCurrency {
                        return CryptoCurrency(parcel)
                }

                override fun newArray(size: Int): Array<CryptoCurrency?> {
                        return arrayOfNulls(size)
                }
        }

        fun isEmpty() = id == null || name == null
}
