package alarm.crypto.cryptoalarm

import alarm.crypto.cryptoalarm.model.Address
import alarm.crypto.cryptoalarm.model.User
import android.app.Application
import com.vicpin.krealmextensions.RealmConfigStore
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.annotations.RealmModule

/**
 * @autor artemsutulov, 23.11.2017.
 */
class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val userAddressConfig = RealmConfiguration.Builder().name("user-db")
                .schemaVersion(1).deleteRealmIfMigrationNeeded().build()
        RealmConfigStore.init(User::class.java, userAddressConfig)
        RealmConfigStore.init(Address::class.java, userAddressConfig)
    }


    /**
     * Example Realm module
     */
    // TODO unused local storage
    @RealmModule(classes = arrayOf(User::class))
    class UserModule

}