package alarm.crypto.cryptoalarm.fragment.menu

import alarm.crypto.cryptoalarm.Cache
import alarm.crypto.cryptoalarm.R
import alarm.crypto.cryptoalarm.adapter.CurrencyRecycleViewAdapter
import alarm.crypto.cryptoalarm.event.OnCurrencyClick
import alarm.crypto.cryptoalarm.presenter.HomeFragmentPresenter
import alarm.crypto.cryptoalarm.presenter.IHomeFragmentPresenter
import alarm.crypto.cryptoalarm.retrofit.CryptoCurrency
import alarm.crypto.cryptoalarm.view.CurrencyActivity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.like.IconType
import com.like.LikeButton
import com.like.OnLikeListener
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.*


class HomeFragment : Fragment(), IHomeFragmentView {

    companion object {
        val ELEMENTS_STACK_TO_LOAD_COUNT = 30
        val CACHED_INSTANCE_CURRENCIES_LIST_KEY = "cached_currencies"
        val CACHED_INSTANCE_ALL_CURRENCIES_LIST_KEY = "cached_all_currencies"
        val ELEMENTS_STACK_KEY = "elements_stack_in_bundle"
    }

    private lateinit var recyclerView: RecyclerView

    private var visibleCurrencies: ArrayList<CryptoCurrency> = arrayListOf()

    private var allCurrencies: ArrayList<CryptoCurrency> = arrayListOf()

    private var currenciesBeforeSort: ArrayList<CryptoCurrency> = arrayListOf()

    private lateinit var layoutManager: LinearLayoutManager

    private lateinit var adapter: CurrencyRecycleViewAdapter

    private lateinit var presenter: IHomeFragmentPresenter

    private lateinit var favoriteButton: LikeButton

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        layoutManager = LinearLayoutManager(context)
        presenter = HomeFragmentPresenter(this)
        adapter = initCurrencyViewAdapter()
        currenciesBeforeSort = arrayListOf()
        if (Cache.isValueExist(CACHED_INSTANCE_ALL_CURRENCIES_LIST_KEY) && allCurrencies.isEmpty()) {
            presenter.loadAllCurrencies()
        } else {
            allCurrencies.addAll(
                    Cache.getFromCache(CACHED_INSTANCE_ALL_CURRENCIES_LIST_KEY)
                            as ArrayList<CryptoCurrency>
            )
        }
        if (arguments != null && arguments!!.get(ELEMENTS_STACK_KEY) != null) {
            visibleCurrencies.addAll(arguments!!.getParcelableArrayList(ELEMENTS_STACK_KEY))
            adapter.notifyDataSetChanged()
        } else {
            presenter.initCurrencies(ELEMENTS_STACK_TO_LOAD_COUNT)
        }
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onStart() {
        super.onStart()
        val view = view
        if (view != null) {
            swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
            swipeRefreshLayout.setOnRefreshListener {
                presenter.refreshCurrencies(visibleCurrencies.count())
            }
            recyclerView = view.findViewById(R.id.home_recycle_view)
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (visibleCurrencies.size == allCurrencies.size) return
                    adapter.loadContent(recyclerView!!, dx, dy)
                }
            })
            favoriteButton = LikeButton(context)
            favoriteButton.setIcon(IconType.Star)
            favoriteButton.setOnLikeListener(initOnFavoriteButtonClickListener())
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        saveVisibleCurrenciesToCache()
        saveAllCurrenciesListToCache()
    }

    override fun onPause() {
        super.onPause()
        saveVisibleCurrenciesToCache()
        saveAllCurrenciesListToCache()
    }

    override fun clearAndAddCurrenciesToArrayAllCurrencies(currencies: ArrayList<CryptoCurrency>) {
        if (allCurrencies.isNotEmpty()) allCurrencies.clear()
        allCurrencies.addAll(currencies)
    }

    override fun clearAndAddAllCurrencies(currencies: ArrayList<CryptoCurrency>) {
        if (visibleCurrencies.isNotEmpty()) visibleCurrencies.clear()
        addMoreContent(currencies, false)
    }

    override fun addMoreContent(currencies: ArrayList<CryptoCurrency>,
                                isNeedToHintLoadingBar: Boolean) {
        if (isNeedToHintLoadingBar) {
            visibleCurrencies.removeAt(visibleCurrencies.size - 1)
            adapter.notifyItemRemoved(visibleCurrencies.size)
            adapter.setLoaded()
        }
        visibleCurrencies.addAll(currencies)
        adapter.notifyDataSetChanged()
    }

    override fun notifyContentRefreshed() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun searchAndUpdateData(searchString: String) {
        val filteredList = arrayListOf<CryptoCurrency>()
        if (searchString.isNotEmpty()) {
            allCurrencies.forEach {
                if (it.name != null && it.name!!.toLowerCase().contains(searchString.toLowerCase()))
                    filteredList.add(it)
            }
            if (filteredList.isNotEmpty()) {
                if (currenciesBeforeSort.isEmpty()) currenciesBeforeSort.addAll(visibleCurrencies)
                visibleCurrencies.clear()
                visibleCurrencies.addAll(filteredList)
                notifyDataSetChanged(adapter)
            }
        } else {
            visibleCurrencies.clear()
            visibleCurrencies.addAll(currenciesBeforeSort)
            notifyDataSetChanged(adapter)
        }
    }

    override fun updateDataForSort(currencies: ArrayList<CryptoCurrency>) {
    }

    override fun showErrorMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun getAdapter() = adapter

    private fun initCurrencyViewAdapter(): CurrencyRecycleViewAdapter {
        return CurrencyRecycleViewAdapter(
                visibleCurrencies,
                allCurrencies,
                presenter,
                object : OnCurrencyClick {
                    override fun onCurrencyItemClick(itemView: View, adapterPosition: Int) {
                        val intent = Intent(context, CurrencyActivity::class.java)
                        intent.putExtra(
                                CurrencyActivity.EXTRA_CURRENCY_NAME, visibleCurrencies[adapterPosition]
                        )
                        startActivity(intent)
                    }
                },
                context!!
        )
    }

    private fun notifyDataSetChanged(adapter: CurrencyRecycleViewAdapter) = Observable.just(adapter)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                    onNext = { it.notifyDataSetChanged() }
            )

    private fun saveVisibleCurrenciesToCache() = Cache
            .replaceIfExists(CACHED_INSTANCE_CURRENCIES_LIST_KEY, visibleCurrencies)

    private fun saveAllCurrenciesListToCache() = Cache
            .replaceIfExists(CACHED_INSTANCE_ALL_CURRENCIES_LIST_KEY, allCurrencies)

    private fun initOnFavoriteButtonClickListener(): OnLikeListener {
        return object : OnLikeListener {
            override fun liked(p0: LikeButton?) {
                TODO("not implemented")
            }

            override fun unLiked(p0: LikeButton?) {
                TODO("not implemented")
            }
        }
    }

}
