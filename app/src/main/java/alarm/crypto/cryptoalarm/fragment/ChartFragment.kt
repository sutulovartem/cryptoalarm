package alarm.crypto.cryptoalarm.fragment

import alarm.crypto.cryptoalarm.R
import alarm.crypto.cryptoalarm.retrofit.CoinMarketCapApiContainer
import alarm.crypto.cryptoalarm.retrofit.CoinMarketCapGraphAdi
import alarm.crypto.cryptoalarm.retrofit.CryptoCurrencyGraphData
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ChartFragment : Fragment() {

    private lateinit var mChart: LineChart

    private lateinit var graphAdi: CoinMarketCapGraphAdi

    private lateinit var graphData: CryptoCurrencyGraphData

    // TODO create great graphics, because now it's a copy-paste
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_chart, container, false)
        mChart = view.findViewById<LineChart>(R.id.chart) as LineChart
        graphAdi = CoinMarketCapApiContainer.getGraphApi()
        mChart.setDrawGridBackground(false)
        mChart.description.isEnabled = false
        mChart.setDrawBorders(false)
        mChart.axisLeft.isEnabled = false
        mChart.axisRight.setDrawAxisLine(false)
        mChart.axisRight.setDrawGridLines(false)
        mChart.xAxis.setDrawAxisLine(false)
        mChart.xAxis.setDrawGridLines(false)
        // enable touch gestures
        mChart.setTouchEnabled(true)
        // enable scaling and dragging
        mChart.isDragEnabled = true
        mChart.setScaleEnabled(true)
        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false)
        val l = mChart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        graphAdi.getGraphDataByCurrencyName("bitcoin-cash")
                .enqueue(object : Callback<CryptoCurrencyGraphData> {

                    override fun onResponse(call: Call<CryptoCurrencyGraphData>?,
                                            response: Response<CryptoCurrencyGraphData>?) {
                        graphData = response!!.body()!!
                        var dataObjects = graphData.priceUsd
                        var entries = arrayListOf<Entry>()

                        dataObjects!!.forEach {
                            entries.add(Entry(Date((it[0])).time.toFloat(), it[1].toFloat()))
                        }
                        var dataSet = LineDataSet(entries, "Label")
                        dataSet.color = Color.BLUE
                        dataSet.valueTextColor = Color.GRAY
                        val lineData = LineData(dataSet)
                        mChart.data = lineData
                        mChart.invalidate() // refresh
                    }

                    override fun onFailure(call: Call<CryptoCurrencyGraphData>?, t: Throwable?) {
                    }

                })
        return view
    }
}
