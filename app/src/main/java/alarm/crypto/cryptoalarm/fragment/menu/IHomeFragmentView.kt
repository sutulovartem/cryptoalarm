package alarm.crypto.cryptoalarm.fragment.menu

import alarm.crypto.cryptoalarm.retrofit.CryptoCurrency

/**
 * @autor artemsutulov, 14.12.2017.
 */
interface IHomeFragmentView {
    fun clearAndAddAllCurrencies(currencies: ArrayList<CryptoCurrency>)
    fun clearAndAddCurrenciesToArrayAllCurrencies(currencies: ArrayList<CryptoCurrency>)
    fun addMoreContent(currencies: ArrayList<CryptoCurrency>, isNeedToHintLoadingBar: Boolean)
    fun notifyContentRefreshed()
    fun searchAndUpdateData(searchString: String)
    fun updateDataForSort(currencies: ArrayList<CryptoCurrency>)
    fun showErrorMessage(message: String)
}