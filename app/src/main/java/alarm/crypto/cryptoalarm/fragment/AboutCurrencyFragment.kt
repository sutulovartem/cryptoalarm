package alarm.crypto.cryptoalarm.fragment

import alarm.crypto.cryptoalarm.R
import alarm.crypto.cryptoalarm.retrofit.CryptoCurrency
import alarm.crypto.cryptoalarm.view.CurrencyActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.lang.String.format

class AboutCurrencyFragment : Fragment() {

    private lateinit var currency: CryptoCurrency

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_currency, container, false)
    }

    override fun onStart() {
        super.onStart()
        val bundle: Bundle? = arguments
        if (bundle != null) {
            currency = bundle.get(CurrencyActivity.CURRENCY_KEY_FOR_FRAGMENT) as CryptoCurrency
            view!!.findViewById<TextView>(R.id.about_currency_currency_title_price).text =
                    format("${format("%.2f", currency.priceUsd?.toDouble())} %s", "USD")
        }
    }
}
