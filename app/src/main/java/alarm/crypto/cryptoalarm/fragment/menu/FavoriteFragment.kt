package alarm.crypto.cryptoalarm.fragment.menu

import alarm.crypto.cryptoalarm.R
import alarm.crypto.cryptoalarm.retrofit.CryptoCurrency
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class FavoriteFragment : Fragment() {

    private lateinit var favoriteCurrencies: ArrayList<CryptoCurrency>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favorite, container, false)

        return view
    }
}
