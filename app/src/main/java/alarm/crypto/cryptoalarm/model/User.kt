package alarm.crypto.cryptoalarm.model

import io.realm.RealmObject
import io.realm.annotations.RealmClass

/**
 * @autor artemsutulov, 23.11.2017.
 */
// TODO it's just a 'hello-world' class for testing realm local storage
@RealmClass
open class User(
        var name: String? = null,
        var address: Address? = Address()
) : RealmObject()

/**
 * @autor artemsutulov, 23.11.2017.
 */
// TODO it's just a 'hello-world' class for testing realm local storage
@RealmClass
open class Address(
        var street: String? = null,
        var city: String? = null,
        var zip: String? = null
) : RealmObject()