package alarm.crypto.cryptoalarm.event

import android.view.View

/**
 * @autor artemsutulov, 16.11.2017.
 */
interface OnCurrencyClick {
    fun onCurrencyItemClick(itemView: View, adapterPosition: Int)
}