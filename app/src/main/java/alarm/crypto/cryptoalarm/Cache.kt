package alarm.crypto.cryptoalarm

import java.util.concurrent.ConcurrentHashMap

/**
 * @autor artemsutulov, 19.12.2017.
 */
class Cache private constructor() {

    companion object {
        private val CACHE = ConcurrentHashMap<String, Any>()


        fun getFromCache(key: String) = CACHE[key]

        fun replaceIfExists(key: String, value: Any) {
            if (CACHE[key] != null) CACHE.remove(key)
            CACHE.put(key, value)
        }

        fun isValueExist(key: String) = CACHE[key] == null
    }
}